<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function bsGetView($view,$param)
    {
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', $view,$param);
        }else {
            return view('master.master')->nest('child', $view,$param);
        }
    }

    public function bsNumberFormat($number)
    {
        return number_format($number,0,',','.');
    }
    
    public function bsNumberFormatDecimal($number,$decimal)
    {
        return number_format($number,$decimal,',','.');
    }

    
    public function checking($type)
    {
        if ($_GET['isEdit']) {

            $data = collect(\DB::select("SELECT * FROM users where id = ".$_GET['isEdit']))->first();

            if ($type == "username") {

                if ($_GET['username'] == $data->username) {
                    $valid = true;
                }else{
                    $data = \DB::select("SELECT * FROM users where username = '".$_GET['username']."'");
                    $valid = (count($data) > 0) ? false : true ;
                }

            }
            elseif ($type == "email") {

                if ($_GET['email'] == $data->email) {
                    $valid = true;
                }else {
                    $data = \DB::select("SELECT * FROM users where email = '".$_GET['email']."'");
                    $valid = (count($data) > 0) ? false : true ;
                }
            }
            elseif ($type == "phone") {

                if ($_GET['phone'] == $data->phone) {
                    $valid = true;
                }else {
                    $data = \DB::select("SELECT * FROM users where phone = '".$_GET['phone']."'");
                    $valid = (count($data) > 0) ? false : true ;
                }
            }



            echo json_encode(array(
                'valid' => $valid,
            ));




        }else {

            if ($type == "username") {
                $data = \DB::select("SELECT * FROM users where username = '".$_GET['username']."'");
            }elseif ($type == "email") {
                $data = \DB::select("SELECT * FROM users where email = '".$_GET['email']."'");
            }elseif ($type == "phone") {
                $data = \DB::select("SELECT * FROM users where phone = '".$_GET['phone']."'");
            }

            $isAvailable = (count($data) > 0) ? false : true ;

            echo json_encode(array(
                'valid' => $isAvailable,
            ));

        }


    }
}
