<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB,Response,Hash,Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

class DummyController extends Controller
{
    public function index(Request $request)
    {
        $param['tittle'] = 'Dummy';
        return $this->bsGetView('content.dummy.index',$param);
    }

    public function data_list()
    {
       $data = \DB::select("SELECT * FROM dummy");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
       
        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item">
                  <i class="la la-edit"></i>
                  <span>Edit</span>
              </a>
              <a class="dropdown-item">
                  <i class="la la-clipboard"></i>
                  <span>Detail</span>
              </a>
            </div>
        </div>
        ';
        })
        ->editColumn('price',function($data) {
            return $this->bsNumberFormat($data->price);
        })
        ->rawColumns(['action'])
        ->make(true);

    }

}
