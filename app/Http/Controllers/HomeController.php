<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //INIT
        $param['get_dashboard'] = route('home.get_dashboard');
        $param['vAction'] = 'content.home.action';

        return $this->bsGetView('content.home.dashboard',$param);
        
    }

    public function get_dashboard(Request $request)
    {
        $f = "";

        // if ($filter == "all") {
        //     $f = "";
        // }
        // elseif ($filter == "t_last_month") {
        //     $f = "  EXTRACT(MONTH FROM ms.created_at) = ".date("m", strtotime("-1 months"))." and EXTRACT(YEAR FROM ms.created_at) = ".date('Y');
        // }
        // elseif ($filter == "t_month") {
        //     $f = " EXTRACT(MONTH FROM ms.created_at) = ".date('m')." and EXTRACT(YEAR FROM ms.created_at) = ".date('Y');
        // }
        // elseif ($filter == "year_t_date") {
        //     $f = " EXTRACT(MONTH FROM ms.created_at) <= ".date('m')." and EXTRACT(MONTH FROM ms.created_at) >= 1
        //     and EXTRACT(YEAR FROM ms.created_at) = ".date('Y');

        // }

        $data['data1'] = collect(\DB::select("select COALESCE(count(id),0) as d from users"))->first();
        $data['data2'] = collect(\DB::select("select COALESCE(count(id),0) as d from users"))->first();
        $data['data3'] = collect(\DB::select("select COALESCE(count(id),0) as d from users"))->first();
        $data['data4'] = collect(\DB::select("select COALESCE(count(id),0) as d from users"))->first();
        $data['data5'] = collect(\DB::select("select COALESCE(count(id),0) as d from users"))->first();

        
        // if ($t_sales) {
        //     foreach ($t_sales as $key => $value) {
        //         $temp_t_sales_name[] = $value->full_name;
        //         $temp_t_sales_data[] = $value->d;
        //         $temp_t_sales_pres[] = $value->presentase;
        //     }
        // }else {
        //     $temp_t_sales_name[] = "-";
        //     $temp_t_sales_data[] = 0;
        //     $temp_t_sales_pres[] = "0%";
        // }

        // if ($t_premi) {
        //     foreach ($t_premi as $key => $value) {
        //         $temp_t_premi_name[] = $value->full_name;
        //         $temp_t_premi_data[] = $value->d;
        //         $temp_t_premi_pres[] = $value->presentase;
        //     }
        // }else {
        //     $temp_t_premi_name[] = "-";
        //     $temp_t_premi_data[] = 0;
        //     $temp_t_premi_pres[] = "0%";
        // }


        // if ($c_segment) {
        //     foreach ($c_segment as $key => $value) {
        //         $temp_seg_def[] = $value->definition;
        //         $temp_seg_data[] = $value->d;
        //         $temp_seg_pres[] = $value->presentase;
        //     }
        // }else {
        //     $temp_seg_def[] = "-";
        //     $temp_seg_data[] = 0;
        //     $temp_seg_pres[] = "0%";
        // }


        // if ($c_customer_type) {
        //     foreach ($c_customer_type as $key => $value) {
        //         $temp_cust_def[] = $value->definition;
        //         $temp_cust_data[] = $value->d;
        //         $temp_cust_pres[] = $value->presentase;
        //     }
        // }else {
        //     $temp_cust_def[] = "-";
        //     $temp_cust_data[] = 0;
        //     $temp_cust_pres[] = "0%";
        // }



        // $data['cust_def'] = $temp_cust_def;
        // $data['cust_data'] = $temp_cust_data;
        // $data['cust_pres'] = $temp_cust_pres;

        // $data['seg_def'] = $temp_seg_def;
        // $data['seg_data'] = $temp_seg_data;
        // $data['seg_pres'] = $temp_seg_pres;

        // $data['t_sales_nama'] = $temp_t_sales_name;
        // $data['t_sales_data'] = $temp_t_sales_data;
        // $data['t_sales_pres'] = $temp_t_sales_pres;

        // $data['t_premi_nama'] = $temp_t_premi_name;
        // $data['t_premi_data'] = $temp_t_premi_data;
        // $data['t_premi_pres'] = $temp_t_premi_pres;



        // $comma = ",";

        for ($bulan=1; $bulan <= 12; $bulan++) {
            $temp_graf_dummy1[] = $bulan;
            $temp_graf_dummy2[] = $bulan+2;
        }

        $data_graf['Data Dummy 1'] = $temp_graf_dummy1;
        $data_graf['Data Dummy 2'] = $temp_graf_dummy2;

        // //LINE GRAFIK
        $data['line_graf'] = $data_graf;

        return json_encode($data);
    }
}
