<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/user/index';

    public function index()
    {
        if (Auth::user()) {           
            return view('user.index');
        }else{
            return view('login');
        }
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');
        $user = DB::table('users')->where('username', $request->input('username'))->first();
        
        if (!is_null($user)) {
            
            if (!Hash::check($request->input('password'), $user->password)){

                return response()->json([
                    'rc' => 0,
                    'rm' => 'Username atau Password salah'
                ]);

            }else if ($user->status_user != 't'){

                return response()->json([
                    'rc' => 1,
                    'rm' => 'Akun anda tidak aktif. Silahkan hubungi admin.'
                ]);

            }else {

                Auth::loginUsingId($user->id);
                return response()->json([
                    'rc' => 2,
                    'rm' => 'success'
                ]);
            }

        } else {
            
            return response()->json([
                'rc' => 0,
                'rm' => 'Username atau Password salah'
            ]);
        }

    }

    public function logout()
    {
        Auth::logout();
        return view('login');
    }
}
