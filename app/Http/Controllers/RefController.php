<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\UserModel;

class RefController extends Controller
{
    // LIST BUTTON
    // edit,active,setting_menu

    public function getInit($request,$route)
    {
        $data['route'] = $route;

        switch ($route) {
            case 'role':
             
            break;

            case 'menu':
                $data['tittle'] = 'Referensi Menu';
                $data['actButton'] = ['setting_menu'];
                
                // INIT DB
                $data['db'] = 'ref_rel_menu';
                $data['db_key'] = 'role_id';
 
                 // LIST DATA TABLE
                $data['data_table'] = \DB::select("SELECT *,role_id as list_menu FROM ref_role_user order by role_id desc");
 
                $data['getMenu'] = \DB::select("SELECT * from ref_menu order by seq");
 
                 // GET DATA FOR EDIT
                 if ($request->post('id')) {
                     $data['get_data_edit'] = \DB::select("SELECT * FROM ref_rel_menu where role_id = ".$request->post('id'));
                 }
             break;
        }

        return $data;
    }
    public function index(Request $request)
    {
        //INIT 
        $init = $this->getInit($request,$request->get('type'));
        $route = $init['route']; 
        $param['tittle'] = $init['tittle'];
        //END INIT 

        $param['table'] = route('ref.data')."?type=".$route;
        $param['edit'] = route('ref.edit')."?type=".$route;
        $param['store'] = route('ref.store')."?type=".$route;
        $param['storeCustom'] = route('ref.storeCustom')."?type=".$route;
        $param['setActive'] = route('ref.setActive')."?type=".$route;

        $param['vForm'] = 'content.ref.'.$route.'.form';
        $param['vAction'] = 'content.ref.'.$route.'.action';
        $param['init'] = $init;

        return $this->bsGetView('content.ref.'.$route.'.index',$param);
    }

    public function data(Request $request)
    {
        //GET INIT 
       $init = $this->getInit($request,$request->get('type'));

       $data = $init['data_table'];
       return DataTables::of($data)
       ->addColumn('action', function ($data) use($init) {

        $param['id_key'] = $data->{$init['db_key']};
        $param['data'] = $data;
        return $this->getButtonAction($init['actButton'],$param);

        // CUSTOM COLUMN
        })
        ->editColumn('is_active',function($data) {
            return ($data->is_active == 't') ? '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>':'<span class="kt-badge kt-badge--danger kt-badge--inline">non-active</span>';
        })

        ->editColumn('list_menu',function($data) use($init) {
            if ($init['route'] == 'menu') {
                $getMenu = \DB::select("SELECT * from ref_rel_menu rrm 
                join ref_menu rm on rm.id = rrm.menu_id 
                where role_id = ? and parent is null",[$data->list_menu]);
                
                $menu_list = '';
                foreach ($getMenu as $key => $v) {
                    $menu_list .= '<span style="margin-right:5px;font-size: 13px;padding: 13px;font-weight: 500;" class="kt-badge kt-badge--success kt-badge--inline">'.$v->menu_name.'</span>'; 
                }
                return $menu_list;
            }
        })
        
        
        ->rawColumns(['is_active','list_menu', 'action'])
        ->make(true);
    }

    public function store(Request $request)
    {
        //GET INIT 
        $init = $this->getInit($request,$request->get('type'));
        $get_id = $request->input('get_id');
        $setField = $init['set_field'];

         //IF EDIT DATA
         if ($get_id) {
            DB::table($init['db'])->where($init['db_key'], $get_id)->update($setField);
        //IF ADD DATA
        }else {
            $seq = DB::table($init['db'])->max($init['db_key']);
            $setField['role_id'] = $seq+1;
            DB::table($init['db'])->insert($setField);
        }

        return response()->json([
            'rc' => 0,
            'rm' => "Berhasil"
        ]);   
    }

    public function storeCustom(Request $request)
    {
        switch ($request->get('type')) {
            case 'menu':

                DB::table('ref_rel_menu')->where('role_id', $request->post('get_id'))->delete();

                $menu_id = $request->post('menu_id');

                if ($menu_id) {
                    foreach ($menu_id as $key => $v) {
                        $seq = DB::table('ref_rel_menu')->max('id');
                    
                        DB::table('ref_rel_menu')->insert([
                            'id' => $seq+1,
                            'role_id' => $request->post('get_id'),
                            'menu_id' => $v
                        ]);
                    }
                }
                
                return response()->json([
                    'rc' => 0,
                    'rm' => "Berhasil"
                ]);  

            break;
            
        }
    }

    public function edit(Request $request)
    {
         //GET INIT 
        $init = $this->getInit($request,$request->get('type'));
        return json_encode($init['get_data_edit']);
    }

    public function SetActive(Request $request)
    {
         //GET INIT 
         $init = $this->getInit($request,$request->get('type'));

        $id = $request->input('id');
        $isActive = $request->input('active');

        if ($isActive == 1) {
            DB::table($init['db'])->where($init['db_key'], $id)->update([
                'is_active' => 'f',
            ]);
            $reqMessage = 'berhasil dinonaktifkan';
        } else {
            DB::table($init['db'])->where($init['db_key'], $id)->update([
                'is_active' => 't',
            ]);
            $reqMessage = 'berhasil diaktifkan';
        }

        return response()->json([
            'rc' => 1,
            'rm' => $reqMessage
        ]);

    }

    public function getButtonAction($typeButton,$param)
    {
        // dd($param['data']->is_active);
        $editBtn ="";
        $ActiveBtn = "";
        $menuBtn = "";

         // EDIT BUTTON
         if (in_array("edit", $typeButton)) {
            $editBtn = ' <button class="dropdown-item" onclick="modalEdit(`'.$param['id_key'].'`)">
                            <i class="la la-edit"></i>
                            <span>Edit</span>
                        </button>'; 
        }

         // ACTIVE BUTTON
         if (in_array("active", $typeButton)) {
            if ($param['data']->is_active == 't') {
                $ActiveBtn = '<button class="dropdown-item" onclick="setActive(`'.$param['id_key'].'`,`'.$param['data']->is_active.'`)">
                     <i class="la la-times-circle-o"></i>
                     <span>Deactived</span>
                 </button>';
             }else{
                 $ActiveBtn = '<button class="dropdown-item" onclick="setActive(`'.$param['id_key'].'`,`'.$param['data']->is_active.'`)">
                     <i class="la la-check-circle-o"></i>
                     <span>Active</span>
                 </button>';
             }
        }

         // SETTING MENU BUTTON
         if (in_array("setting_menu", $typeButton)) {
            $menuBtn = ' <button class="dropdown-item" onclick="modalMenu(`'.$param['id_key'].'`)">
                            <i class="la la-list-ul"></i>
                            <span>Akses Menu</span>
                        </button>'; 
        }

        $resultBtn = $editBtn.$ActiveBtn.$menuBtn;

        return '
            <div class="dropdown dropdown-inline">
                <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="flaticon-more"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-center">
                '.$resultBtn.'
                </div>
            </div>
        ';
    }
    
}
