<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\UserModel;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        //INIT
        $route = 'role';
        $param['tittle'] = 'Referensi Role';

        $param['table'] = route($route.'.data');
        $param['edit'] = route($route.'.edit');
        $param['store'] = route($route.'.store');
        $param['setActive'] = route($route.'.setActive');

        $param['vForm'] = 'content.'.$route.'.form';
        $param['vAction'] = 'content.'.$route.'.action';

        return $this->bsGetView('content.'.$route.'.index',$param);
    }

    public function data()
    {
       $data = \DB::select("SELECT * FROM ref_role_user");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {

         if ($data->is_active == 't') {
            $actStatus = '<button class="dropdown-item" onclick="setActive(`'.$data->role_id.'`,`'.$data->is_active.'`)">
                 <i class="la la-times-circle-o"></i>
                 <span>Deactived</span>
             </button>';
         }else{
             $actStatus = '<button class="dropdown-item" onclick="setActive(`'.$data->role_id.'`,`'.$data->is_active.'`)">
                 <i class="la la-check-circle-o"></i>
                 <span>Active</span>
             </button>';
         }

        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-center">
              <button class="dropdown-item" onclick="modalEdit(`'.$data->role_id.'`)">
                  <i class="la la-edit"></i>
                  <span>Edit</span>
              </button>'.$actStatus.'

            </div>
        </div>
        ';
        })
        ->editColumn('is_active',function($data) {
            return ($data->is_active == 't') ? '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>':'<span class="kt-badge kt-badge--danger kt-badge--inline">non-active</span>';
        })
        ->rawColumns(['is_active', 'action'])
        ->make(true);
    }

    public function store(Request $request)
    {
        $get_id = $request->input('get_id');

        //IF ADD DATA
        if ($get_id) {
            $data = UserModel::find($get_id);
        
        //IF EDIT DATA
        }else {
            $get = collect(\DB::select("SELECT max(id::int) as max_id FROM users"))->first();
            $data = new UserModel();
            $data->id = $get->max_id+1;
            $data->status_user = 't';
            $data->password = Hash::make('admin');
        }

        $data->username = $request->input('username');
        $data->name = $request->input('name');
        $data->alamat = $request->input('alamat');
        $data->email = $request->input('email');
        $data->phone = $request->input('phone');
        $data->role = $request->input('role');
        $data->save();

        if($data){
            return response()->json([
                'rc' => 0,
                'rm' => "Berhasil"
            ]);
        }else{
            return response()->json([
                'rc' => 99,
                'rm' => "Terjadi Kesalahan, Proses Dibatalkan"
            ]);
        }
    }

    public function edit(Request $request)
    {
        $data = \DB::select("SELECT * FROM ref_role_user where role_id = '".$request->input('id')."'");
        return json_encode($data);
    }

    public function SetActive(Request $request)
    {
        $table = 'ref_role_user';
        $id = $request->input('id');
        $isActive = $request->input('active');

        if ($isActive == 1) {
            DB::table($table)->where('role_id', $id)->update([
                'is_active' => 'f',
            ]);
            $reqMessage = 'berhasil dinonaktifkan';
        } else {
            DB::table($table)->where('role_id', $id)->update([
                'is_active' => 't',
            ]);
            $reqMessage = 'berhasil diaktifkan';
        }

        return response()->json([
            'rc' => 1,
            'rm' => $reqMessage
        ]);

    }

    
}
