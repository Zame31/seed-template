<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\UserModel;

class UserController extends Controller
{
    public function index(Request $request)
    {
        //INIT
        $param['table'] = route('user.data');
        $param['edit'] = route('user.edit');
        $param['store'] = route('user.store');
        $param['setActive'] = route('user.setActive');
        $param['resetPassword'] = route('user.resetPassword');

        // INIT VIEW
        $param['tittle'] = 'Referensi User';
        $param['vForm'] = 'content.user.form';
        $param['vAction'] = 'content.user.action';

        $param['dr'] = \DB::table('ref_role_user')->get();

        return $this->bsGetView('content.user.index',$param);
    }

    public function data()
    {
       $data = \DB::select("SELECT * FROM users u JOIN ref_role_user ru on u.role = ru.role_id order by u.id");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {

        if ($data->status_user == 't') {
           $actStatus = '<button class="dropdown-item" onclick="setActive(`'.$data->id.'`,`'.$data->status_user.'`)">
                <i class="la la-times-circle-o"></i>
                <span>Deactived</span>
            </button>';
        }else{
            $actStatus = '<button class="dropdown-item" onclick="setActive(`'.$data->id.'`,`'.$data->status_user.'`)">
                <i class="la la-check-circle-o"></i>
                <span>Active</span>
            </button>';
        }

        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-center">
              <button class="dropdown-item" onclick="modalEdit(`'.$data->id.'`)">
                  <i class="la la-edit"></i>
                  <span>Edit</span>
              </button>'.$actStatus.'

              <button class="dropdown-item" onclick="resetPassword(`'.$data->id.'`)">
                    <i class="la la-key"></i>
                    <span>Reset Password</span>
                </button>
            </div>
        </div>
        ';
        })
        ->editColumn('status_user',function($data) {
            return ($data->status_user == 't') ? '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>':'<span class="kt-badge kt-badge--danger kt-badge--inline">non-active</span>';
        })
        ->rawColumns(['status_user', 'action'])
        ->make(true);
    }

    public function store(Request $request)
    {
        $get_id = $request->input('get_id');

        //IF EDIT DATA
        if ($get_id) {
            $data = UserModel::find($get_id);
        
        //IF ADD DATA
        }else {
            $get = collect(\DB::select("SELECT max(id::int) as max_id FROM users"))->first();
            $data = new UserModel();
            $data->id = $get->max_id+1;
            $data->status_user = 't';
            $data->password = Hash::make('admin');
        }

        $data->username = $request->input('username');
        $data->name = $request->input('name');
        $data->alamat = $request->input('alamat');
        $data->email = $request->input('email');
        $data->phone = $request->input('phone');
        $data->role = $request->input('role');
        $data->save();

        if($data){
            return response()->json([
                'rc' => 0,
                'rm' => "Berhasil"
            ]);
        }else{
            return response()->json([
                'rc' => 99,
                'rm' => "Terjadi Kesalahan, Proses Dibatalkan"
            ]);
        }
    }

    public function edit(Request $request)
    {
        $data = \DB::select("SELECT * FROM users where id = '".$request->input('id')."'");
        return json_encode($data);
    }

    public function delete($id)
    {
        UserModel::destroy($id);
    }

    public function SetActive(Request $request)
    {
        $id = $request->input('id');
        $isActive = $request->input('active');

        if ($isActive == 't') {
            DB::table('users')->where('id', $id)->update([
                'status_user' => 'f',
            ]);
            $reqMessage = 'User berhasil dinonaktifkan';
        } else {
            DB::table('users')->where('id', $id)->update([
                'status_user' => 't',
            ]);
            $reqMessage = 'User berhasil diaktifkan';
        }

        return response()->json([
            'rc' => 1,
            'rm' => $reqMessage
        ]);

    }

    public function resetPassword(Request $request)
    {
        $user = collect(\DB::select("SELECT * FROM users where id = ".$request->input('id')))->first();
        $event = "Reset Password User ".$user->username;

        $data = UserModel::find($request->input('id'));
        $data->password = Hash::make('admin');
        $data->save();

        return response()->json([
            'rc' => 1,
            'rm' => $event
        ]);

    }


    function changePassword(Request $request)
    {
        $user = $this->getUserAdmin(Auth::user()->id);
        // $event = "Ubah Password User ".$user[0]->username;
        // $this->auditTrail($event,"User Admin");

        $data = UserModel::find(Auth::user()->id);
        $data->passwd = Hash::make($request->input('newpassword'));
        $data->save();

        return response()->json([
            'rc' => 1,
        ]);
    }

    public function check_password()
    {
        $data = collect(\DB::select("SELECT passwd FROM sam_users where id = ".Auth::user()->id))->first();

        if(Hash::check('admin',$data->passwd)){
            $result = '1';
        }else {
            $result = '0';
        }

        return json_encode($result);
    }
}
