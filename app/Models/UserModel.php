<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class UserModel extends Model
{

    protected $primaryKey = 'id';
    protected $table = 'users';

    public function getRoleInit()
    {
        $data['tittle'] = 'Referensi Role';
        $data['actButton'] = ['edit','active'];
        
        // INIT DB
        $data['db'] = 'ref_role_user';
        $data['db_key'] = 'role_id';

         // LIST DATA TABLE
        $data['data_table'] = \DB::select("SELECT * FROM ref_role_user order by role_id desc");

         //  FORM FIELD FOR STORE
        $data['set_field'] = [
             'role_name' => $request->post('role_name'),
             'is_active' => $request->post('is_active')
         ];

         // GET DATA FOR EDIT
         if ($request->post('id')) {
             $data['get_data_edit'] = \DB::select("SELECT * FROM ref_role_user where role_id = ".$request->post('id'));
         }
    }

}
