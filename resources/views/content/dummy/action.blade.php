<script>
// DATATABLE
let act_url = "{{ route('dummy.data_list') }}";
let colum = [
                { data: 'action'},
                { data: 'name'},
                { data: 'phone'},
                { data: 'price'}
            ];
let columStyle = [
                    { targets: [3], className: 'text-right' },
                    { "orderable": false, "targets": 0 }
                ];
bsGetTable(act_url,colum,columStyle);
// END DATATABLE

</script>