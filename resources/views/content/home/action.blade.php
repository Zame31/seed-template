
<script>
    get_dashboard("year_t_date");
    
    
    function get_dashboard(filter) {
    
            // INIT
            $('#cust_presentase').html('');
            $('#seg_presentase').html('');
            $('#top_sales').html('');
            $('#top_premi').html('');

            var data_graf =[];
            var data_color = ["#fd316254","#F4516C","#FFB822","#8859E0","#0C5484","#66BB6A","#00838F","#e57373"];
            var data_bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
    
            $.ajax({
            url: '{{$get_dashboard}}',
            type: 'POST',
            data: {filter : filter},
               beforeSend: function() {
                  bsLoadingPage();
               },
            success: function (response) {
                var res = $.parseJSON(response);

                // SET DATA HEADER
                $('#data1').html(bsNumFormat(res.data1.d));
                $('#data2').html(bsNumFormat(res.data2.d));
                $('#data3').html(bsNumFormat(res.data3.d));
                $('#data4').html(bsNumFormat(res.data4.d));
                $('#data5').html(bsNumFormat(res.data5.d));
    
                // SET DATA LINE GRAF
                var color_i = 0;
                $.each(res.line_graf, function (k,v) {
                    data_graf.push({
                        label: k,
                        data: v,
                        fill: false,
                        backgroundColor: data_color[color_i],
                        borderColor: data_color[color_i],
                    });
                    color_i++;
                });

                var graf = document.getElementById("graf");
                var myChart = new Chart(graf, {
                    type: 'bar',
                    data: {
                    labels: data_bulan,
                    datasets: data_graf
                },
                    options: {
                                responsive: true,
                                title: {
                                    display: false,
                                    text: 'Grafik Line'
                                },
                                tooltips: {
                                    mode: 'index',
                                    intersect: false,
                                },
                                hover: {
                                    mode: 'nearest',
                                    intersect: true
                                },
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Bulan'
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Frekuensi'
                                        }
                                    }]
                                }
                            }
                });
    
                
                var btn_color = ['brand','warning','danger'];
    
                // $.each(data['cust_pres'], function (k,v) {
    
                //     $('#cust_presentase').append(`
                //         <div class="kt-widget14__legend">
                //             <span class="kt-widget14__bullet" style="background:`+data_color[k]+`"></span>
                //             <span class="kt-widget14__stats">`+parseFloat(v).toFixed(2)+` % `+data['cust_def'][k]+`</span>
                //         </div>
                //     `);
    
                // });
    
                // $.each(data['seg_pres'], function (k,v) {
    
                //     $('#seg_presentase').append(`
                //         <div class="kt-widget14__legend">
                //             <span class="kt-widget14__bullet" style="background:`+data_color[k]+`"></span>
                //             <span class="kt-widget14__stats">`+parseFloat(v).toFixed(2)+` % `+data['seg_def'][k]+`</span>
                //         </div>
                //     `);
    
                // });
    
                // $.each(data['t_sales_data'], function (k,v) {
    
                //     $('#top_sales').append(`
                //         <div class="kt-widget4__item">
                //             <div class="kt-widget4__pic kt-widget4__pic--pic">
                //                 <img src="assets/media/users/default.jpg" alt="">
                //             </div>
                //             <div class="kt-widget4__info">
                //                 <a href="#" class="kt-widget4__username">
                //                     `+data['t_sales_nama'][k]+`
                //                 </a>
                //                 <p class="kt-widget4__text">
                //                     `+numFormatNew( parseFloat(data['t_sales_data'][k]).toFixed(2))+` IDR
                //                 </p>
                //             </div>
                //             <a href="#" class="btn btn-sm btn-label-`+btn_color[k]+` btn-bold">`+parseFloat(data['t_sales_pres'][k]).toFixed(2)+`% to Overall Sales</a>
    
                //         </div>
                //     `);
    
                // });
    
                // $.each(data['t_premi_data'], function (k,v) {
    
                //     $('#top_premi').append(`
                //         <div class="kt-widget4__item">
                //             <div class="kt-widget4__pic kt-widget4__pic--pic">
                //                 <img src="assets/media/users/default.jpg" alt="">
                //             </div>
                //             <div class="kt-widget4__info">
                //                 <a href="#" class="kt-widget4__username">
                //                     `+data['t_premi_nama'][k]+`
                //                 </a>
                //                 <p class="kt-widget4__text">
                //                 `+numFormatNew( parseFloat(data['t_premi_data'][k]).toFixed(2))+` IDR
                //                 </p>
                //             </div>
                //             <a href="#" class="btn btn-sm btn-label-`+btn_color[k]+` btn-bold">`+parseFloat(data['t_premi_pres'][k]).toFixed(2)+`% to Overall Premi</a>
    
                //         </div>
                //     `);
    
                // });
    
    
    
               
    
    
                // var ctx = document.getElementById('sales_customer').getContext('2d');
                // var chart = new Chart(ctx, {
                //     type: 'doughnut',
                //     data: {
                //         labels: data['cust_def'],
                //         datasets: [{
                //             backgroundColor: data_color,
                //             data: data['cust_data']
                //         }]
                //     },
                //     options: {
                //         legend: {
                //             display: false
                //         },
                //         scaleLabel: function(label){return label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
                //     }
                // });
    
    
                // var ctx = document.getElementById('sales_segment').getContext('2d');
                // var chart = new Chart(ctx, {
                //     type: 'doughnut',
                //     data: {
                //         labels: data['seg_def'],
                //         datasets: [{
                //             backgroundColor: data_color,
                //             data: data['seg_data']
                //         }]
                //     },
                //     options: {
                //         legend: {
                //             display: false
                //         }
                //     }
                // });
    
            }
        }).done(function( msg ) {
            bsLoadingPageEnd();
        });
        }
    
    
    
    </script>