<script>
    // INIT
    var routeTable = "{{$table}}";
    var routeStore = "{{$storeCustom}}";
    
    var colum = [
                    { data: 'action'},
                    { data: 'role_name'},
                    { data: 'list_menu'}
                ];            
    var columStyle = 
                [
                    { "orderable": false, "targets": 0 }
                ];
                
    var table = bsGetTable(routeTable,colum,columStyle);
    // INIT
        
    // INIT FORM
    function clearFrom() { 
        $('#is_active').val('').trigger('change');
        $('#get_id').val('');
        $("#form-data")[0].reset();
        $('#form-data').bootstrapValidator("resetForm", true);
    }
    
    
    // STORE ADD AND EDIT
    function saveData() {
        bsStoreForm(routeStore,routeTable,table);
    }
    
    function modalMenu(id) {
        $('#modal').modal('show');
        $('#title_modal').html("Setting Akses Menu");
    
        clearFrom();
    
    
        $.ajax({
            url: "{{$edit}}",
            type: 'POST',
            data:{id:id},
            beforeSend: function() {
                bsLoadingModal();
            },
            success: function (res) {
              $('#get_id').val(id);  
                console.log(res);
                
                var data = $.parseJSON(res);
                $.each(data, function (k,v) {
                    $('#'+v.menu_id).prop('checked', 'checked');
                });
            }
        }).done(function(msg) {
            bsLoadingModalEnd();
        }).fail(function(msg) {
            bsLoadingModalEnd();
        });
    }
    
    
function setChild(id,check) {    
    $('.c'+id).prop('checked', check);
}

function setParent(id,check) {
    var sThisVal = 0;
    $('input:checkbox.c'+id).each(function () {
       sThisVal += parseInt(this.checked ? 1 : 0);      
    });

    if (sThisVal > 0) {
        $('.p'+id).prop('checked', true);
    }else {
        $('.p'+id).prop('checked', false);
    }
}  

    </script>
    