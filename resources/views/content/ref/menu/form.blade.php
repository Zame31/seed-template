<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            
            <div class="modal-body">
                <form id="form-data" class="bs-form">
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-12">

                        <!--begin::Accordion-->
                        <div class="accordion accordion-light  accordion-toggle-arrow">
                            @foreach ($init['getMenu'] as $p)
                                @if ($p->parent == null)
                                    <div class="card">
                                        <div class="card-header">
                                            <div>
                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success" style="margin-bottom: 14px;position: absolute;top: 14px;">
                                                    <input id="{{$p->id}}" value="{{$p->id}}" name="menu_id[]" class="p{{$p->id}}" onchange="setChild({{$p->id}},this.checked);" type="checkbox">
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="card-title collapsed" style="margin-left: 27px;" data-toggle="collapse" data-target="#vm{{$p->id}}">
                                               
                                                <div style="margin-right: 10px;">{{$p->menu_name}}</div> 
                                            </div>
                                        </div>
                                        <div id="vm{{$p->id}}" class="collapse hide">
                                            <div class="card-body">
                                                @foreach ($init['getMenu'] as $sub)
                                                    @if ($sub->parent == $p->id)
                                                 
                                                        <ul class="zn-list-menu">
                                                            <li>
                                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success" style="margin-bottom: 14px;">
                                                                    <input id="{{$sub->id}}" value="{{$sub->id}}" name="menu_id[]" onchange="setParent({{$p->id}},this.checked);" class="c{{$p->id}}" type="checkbox">
                                                                    <span></span>
                                                                </label>
                                                                {{$sub->menu_name}}
                                                            </li>
                                                        </ul>
                                                    @endif
                                                @endforeach

                                            </div>
                                        </div>
                                    </div> 
                                @endif
                            @endforeach
                            
                        </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button id="submitBtn" type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
    </div>
</div>
