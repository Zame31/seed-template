<script>
// INIT
var routeTable = "{{$table}}";
var routeStore = "{{$store}}";

var colum = [
                { data: 'action'},
                { data: 'is_active'},
                { data: 'role_name'}
            ];            
var columStyle = 
            [
                { "orderable": false, "targets": 0 }
            ];
            
var table = bsGetTable(routeTable,colum,columStyle);
// INIT
    
// INIT FORM
function clearFrom() { 
    $('#is_active').val('').trigger('change');
    $('#get_id').val('');
    $("#form-data")[0].reset();
    $('#form-data').bootstrapValidator("resetForm", true);
}


// STORE ADD AND EDIT
function saveData() {
    bsStoreForm(routeStore,routeTable,table);
}

function modalEdit(id) {
    $('#modal').modal('show');
    $('#title_modal').html("Edit Data");

    clearFrom();


    $.ajax({
        url: "{{$edit}}",
        type: 'POST',
        data:{id:id},
        beforeSend: function() {
            bsLoadingModal();
        },
        success: function (res) {
           
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {
                var is_active = (v.is_active == true) ? "t" : "f";

                $('#get_id').val(id);
                $('#role_name').val(v.role_name);
                $('#is_active').val(is_active).trigger('change');
            });
        }
    }).done(function(msg) {
        bsLoadingModalEnd();
    }).fail(function(msg) {
        bsLoadingModalEnd();
    });
}


function setActive(id, isActive) {
    if (isActive == 't') {
        var titleSwal = 'Non Aktif User';
        var textSwal = 'Anda yakin akan menonaktifkan user ini?';
    } else {
        var titleSwal = 'Aktif User';
        var textSwal = 'Anda yakin akan mengaktifkan user ini?';
    }
    swal.fire({
        title: titleSwal,
        text: textSwal,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        reverseButtons: true
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: '{{$setActive}}',
                data: {
                    id: id,
                    active: isActive
                },
                beforeSend: function () {
                    bsLoadingPage();
                },
                success: function (data) {
                    bsLoadingPageEnd();
                    if (data.rc == 1) {
                        toastr.success(data.rm);
                    } else {
                        toastr.error(data.rm);
                    }
                }
            }).done(function (msg) {
                bsLoadingPageEnd();
                table.ajax.url(routeTable).load();
            }).fail(function (msg) {
                bsLoadingPageEnd();
                toastr.error("Terjadi Kesalahan");
                table.ajax.url(routeTable).load();
            });
        }
    });

} 

//VALIDASI
$(document).ready(function () {
    $("#form-data").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            role_name: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                }
            },
            is_active: {
            validators: {
                notEmpty: {
                    message: 'Tidak Boleh Kosong'
                }
            }
        },
            
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });

});



</script>
