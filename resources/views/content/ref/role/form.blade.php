<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            
            
            <div class="modal-body">
                <form id="form-data" class="bs-form">
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Role</label>
                                <input type="text" class="form-control" id="role_name" name="role_name">
                            </div>
                            <div class="form-group">
                                <label for="single">Status</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2" name="is_active" id="is_active">
                                       <option value=""></option>
                                       <option value="t">Aktif</option> 
                                       <option value="f">Tidak Aktif</option> 
                                       
                                    </select>
                                </div>
                            </div>
                         
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button id="submitBtn" type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
    </div>
</div>
