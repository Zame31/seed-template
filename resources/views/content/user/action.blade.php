<script>
// INIT
var routeTable = "{{$table}}";
var routeStore = "{{$store}}";

var colum = [
                { data: 'action'},
                { data: 'status_user'},
                { data: 'username'},
                { data: 'name'},
                { data: 'role_name'}
            ];            
var columStyle = 
            [
                { "orderable": false, "targets": 0 }
            ];
            
var table = bsGetTable(routeTable,colum,columStyle);
// INIT
    
// INIT FORM
function clearFrom() {
    $('#role').val('1000').trigger('change');
    $('#get_id').val('');
    $("#form-data")[0].reset();
    $('#form-data').bootstrapValidator("resetForm", true);
}

// STORE ADD AND EDIT
function saveData() {
    bsStoreForm(routeStore,routeTable,table);
}

function modalEdit(id) {
    $('#modal').modal('show');
    $('#title_modal').html("Edit Data");

    clearFrom();

    $.ajax({
        url: "{{$edit}}",
        type: 'POST',
        data:{id:id},
        beforeSend: function() {
            bsLoadingModal();
        },
        success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {
                $('#get_id').val(v.id);
                $('#username').val(v.username);
                $('#name').val(v.name);
                $('#alamat').val(v.alamat);
                $('#email').val(v.email);
                $('#phone').val(v.phone);
                $('#role').val(v.role).trigger('change');
            });
        }
    }).done(function(msg) {
        bsLoadingModalEnd();
    }).fail(function(msg) {
        bsLoadingModalEnd();
    });
}

function resetPassword(id) {

    swal.fire({
        title: 'Reset Password',
        text: "Anda Yakin akan melakukan reset password?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        reverseButtons: true
    }).then(function(result){
        if (result.value) {
        
            $.ajax({
                type: "POST",
                url: '{{$resetPassword}}',
                data: {
                    id: id
                },
                beforeSend: function () {
                    bsLoadingPage();
                },
                success: function(msg) {
                    toastr.success(msg.rm);
                    $('#modal').modal('hide');
                }
            }).done(function( msg ) {
                bsLoadingPageEnd();
                table.ajax.url(routeTable).load();
            }).fail(function(msg) {
                bsLoadingPageEnd();
                toastr.error("Gagal Direset");
            });

        } else if (result.dismiss === 'cancel') {
            swal('Dibatalkan','Password Tidak Direset','error')
        }
    });

}

function setActive(id, isActive) {
    if (isActive == 't') {
        var titleSwal = 'Non Aktif User';
        var textSwal = 'Anda yakin akan menonaktifkan user ini?';
    } else {
        var titleSwal = 'Aktif User';
        var textSwal = 'Anda yakin akan mengaktifkan user ini?';
    }
    swal.fire({
        title: titleSwal,
        text: textSwal,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        reverseButtons: true
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: '{{$setActive}}',
                data: {
                    id: id,
                    active: isActive
                },
                beforeSend: function () {
                    bsLoadingPage();
                },
                success: function (data) {
                    bsLoadingPageEnd();
                    if (data.rc == 1) {
                        toastr.success(data.rm);
                    } else {
                        toastr.error(data.rm);
                    }
                }
            }).done(function (msg) {
                bsLoadingPageEnd();
                table.ajax.url(routeTable).load();
            }).fail(function (msg) {
                bsLoadingPageEnd();
                toastr.error("Terjadi Kesalahan");
                table.ajax.url(routeTable).load();
            });
        }
    });

} 

$(document).ready(function () {
    $("#form-data").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    stringLength: {
                          max:40,
                          message: 'Maksimal 40 Karakter'
                      },
                    remote: {
                    message: 'Username Telah Digunakan',
                    url: "{{ route('valid.checking','username') }}",
                    data: function(validator) {
                            return {
                                isEdit: validator.getFieldElements('get_id').val()
                            };
                        }
                    }
                }
            },
            name: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    stringLength: {
                          max:80,
                          message: 'Maksimal 80 Karakter'
                      }
                }
            },
            role: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                }
            },
            alamat: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                },
                stringLength: {
                    max:128,
                    message: 'Maksimal 128 Karakter'
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    emailAddress: {
                        message: 'format email salah'
                    },
                    remote: {
                        message: 'Email Telah Digunakan',
                        url: "{{ route('valid.checking','email') }}",
                        data: function(validator) {
                                return {
                                    isEdit: validator.getFieldElements('get_id').val()
                                };
                            }
                    }
                },
                stringLength: {
                          max:80,
                          message: 'Maksimal 80 Karakter'
                      }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    stringLength: {
                        max:16,
                        message: 'Maksimal 16 Karakter'
                    },
                    remote: {
                    message: 'Phone Telah Digunakan',
                    url: "{{ route('valid.checking','phone') }}",
                    data: function(validator) {
                            return {
                                isEdit: validator.getFieldElements('get_id').val()
                            };
                        }
                    }
                },

            },
            role_id: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                }
            }   
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });

});



</script>
