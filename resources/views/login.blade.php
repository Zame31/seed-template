<!DOCTYPE html>
<html lang="en">
<head>
    <base href="">
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SEED TEMPLATE</title>
    <meta name="description" content="Login">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/socicon/css/socicon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/line-awesome/css/line-awesome.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/pages/login/login-1.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/bootstrap-validator/bootstrapValidator.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/myStyle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/loading.css')}}" rel="stylesheet" type="text/css" />
    
    <script src="{{asset('assets/plugins/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/block-ui/jquery.blockUI.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/bootstrap-validator/bootstrapValidator.js')}}" type="text/javascript"></script>
    

</head>

<!-- Loading -->
  <div class="bsloader-container" style="display:none;">
    <div class="bsprogress bsfloat bsshadow">
      <div class="bsprogress__item"></div>
    </div>
  </div>

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
            <div
                class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">

                <!--begin::Aside-->
                <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside"
                    style="background-image: url('{{asset('img/background_spec.jpg')}}');">
                    <div class="kt-grid__item">
                        <a href="#" class="kt-login__logo">
                        </a>
                    </div>
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                        <div class="kt-grid__item kt-grid__item--middle">
                            <h3 class="kt-login__title">Welcome Aplication Name</h3>
                            <h4 class="kt-login__subtitle">Aplication Name Description</h4>
                        </div>
                    </div>
                    <div class="kt-grid__item">
                        <div class="kt-login__info">
                            <div class="kt-login__copyright">
                                &copy 2020
                            </div>

                        </div>
                    </div>
                </div>
                <div
                    class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
                    <div class="kt-login__body">
                        <div class="kt-login__form">
                            <div class="kt-login__title">
                                <h3>Sign In</h3>
                            </div>
                            <form class="kt-form" id="loginForm">
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                <!-- Alert Error -->
                                <div class="alert alert-danger alert-dismissible d-none" id="alertError">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                                </div>

                                <!-- Alert Info -->
                                <div class="alert alert-info alert-dismissible d-none" id="alertInfo">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                                </div>

                                <!-- Alert Success -->
                                <div class="alert alert-success alert-dismissible d-none" id="alertSuccess">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                                </div>

                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Username" name="username"
                                        autocomplete="off" id="inputUsername">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="password" placeholder="Password" name="password"
                                        autocomplete="off" id="inputPassword">
                                </div>

                                <div class="kt-login__actions text-right">
                                    <button onclick="login();" id="kt_login_signin_submit"
                                        class="btn btn-primary btn-elevate kt-login__btn-primary">Sign In</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    // enable enter in form login
    $('#loginForm').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            login();
        }
    });

    $(document).ready(function () {
        // validate login
        $("#loginForm").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                username: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });

    function login(){
        var validateLogin = $('#loginForm').data('bootstrapValidator').validate();
        if (validateLogin.isValid()) {

            var formData = document.getElementById("loginForm");
            var objData = new FormData(formData);

            $.ajax({
                type: 'POST',
                url: '{{ route('login.action') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    $('.bsloader-container').fadeIn();
                    $("#alertInfo").addClass('d-none');
                    $("#alertError").addClass('d-none');
                    $("#alertSuccess").addClass('d-none');
                    $("#loading").css('display', 'block');
                },

                success: function (response) {
                    // $('.bsloader-container').fadeOut();
                    // $("#loading").css('display', 'none');
                    $('.bsloader-container').fadeOut();
                    switch (response.rc) {
                        // password / username invalid
                        case 0:
                            $("#alertError").removeClass('d-none');
                            $("#alertError").text(response.rm);
                        break;

                        // akun tidak aktif
                        case 1:
                            $("#alertInfo").removeClass('d-none');
                            $("#alertInfo").text(response.rm);
                        break;

                        // login success
                        case 2:
                            window.location.href = '{{ route('user.index') }}';
                        break;
                    }
                }

            }).done(function (msg) {
                // $('.bsloader-container').fadeOut();
            }).fail(function (msg) {
                $('.bsloader-container').fadeOut();
                $("#alertInfo").text("Koneksi Bermasalah, Gagal Login");
            });
        }
    }

</script>

</body>

</html>
