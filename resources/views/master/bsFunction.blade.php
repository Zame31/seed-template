<script>
var base_url = "{{ url('/') }}"+"/";

var KTBootstrapDatepicker = function () {
    return {
        init: function() {
            $('.init-date').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
            });

            $('.init-select2').select2({
                placeholder: "Silahkan Pilih"
            });

            // $.sessionTimeout({
            //     title: 'Session Timeout Notification',
            //     message: 'Your session is about to expire.',
            //     logoutButton:false,
            //     warnAfter: (60000*10), //warn after 5 seconds
            //     redirAfter: (61000*10), //redirect after 10 secons,
            //     countdownMessage: 'Redirecting in {timer} seconds.',
            //     countdownBar: true,
            //     onRedir: function () {
            //         endSession();
            //     }
            // });

            avatar = new KTAvatar('kt_user_add_avatar');
        }
    };
}();

jQuery(document).ready(function() {
    KTBootstrapDatepicker.init();
});


$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

function bsLoadMenu(page) {
  $.ajax({
      type: "GET",
      url: base_url+page,
      data: {},
      beforeSend: function () {
          
          $('#pageLoad').fadeOut();
          bsLoadingPage();
      },
      success: function (data) {
          $("#kt_aside_close_btn").click();
      },
      
  }).done(function (data) {
      bsLoadingPageEnd();
      var state = { name: page, page: page, url: base_url+page };
      window.history.pushState(state, "History", base_url+page);
      $('#pageLoad').html(data).fadeIn();
      KTBootstrapDatepicker.init();
      
  });
}

$(document).ready(function () {
    
    window.onpopstate = function() {
        $.ajax({
            type: "GET",
            url: window.location.href,
            data: {},
            beforeSend: function () {
                
                $('#pageLoad').fadeOut();
                bsLoadingPage();
            },
            success: function (data) {
                $("#kt_aside_close_btn").click();
            },
            
        }).done(function (data) {
            bsLoadingPageEnd();
            var state = { name: "name", page: 'History', url: window.location.href };
            window.history.pushState(state, "History", window.location.href);
            $('#pageLoad').html(data).fadeIn();
            KTBootstrapDatepicker.init();
            
        });
        
        
    };

    $('.bs-form').on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            saveData();
        }
    });
});

function bsModalAdd() {
    $('#title_modal').html("Tambah Data");
    $('#modal').modal('show');
    
}

function bsGetTable(dAct,dColum,dColumStyle) {
    var table = $('#bs-dt').DataTable({
        aaSorting: [],
        processing: true,
        serverSide: true,
        columnDefs: dColumStyle,
        ajax: {
            "url" : dAct,
            "error": function(jqXHR, textStatus, errorThrown)
                {
                    toastr.warning("Terjadi Kesalahan Saat Pengambilan Data !");
                }
            },
        columns: dColum
    });

    return table;
}

function bsStoreForm(store,tableView,table) {
    var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {

        var formData = document.getElementById("form-data");
        var objData = new FormData(formData);

        $.ajax({
            type: 'POST',
            url: store,
            data: objData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                bsLoadingModal();
                 
            },
            success: function (response) {
                bsLoadingModalEnd();
                $('#modal').modal('hide');

                if (response.rc == 0) {
                    toastr.success(response.rm);
                }else {
                    toastr.warning(response.rm);
                }
            }

        }).done(function (msg) {
            table.ajax.url(tableView).load();
        }).fail(function (msg) {
            bsLoadingModalEnd();
            toastr.error("Terjadi Kesalahan");
        });

    }
}

// LOADING FUNCTION
function bsLoadingPage() {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}

function bsLoadingModal() {
    $('.bsloader-container').fadeIn();
}

function bsLoadingModalEnd() {
    $('.bsloader-container').fadeOut();
}

function bsLoadingPageEnd() {
    KTApp.unblockPage();
}
// LOADING FUNCTION 

function bsKeyuppToNumber(objek) {
    var separator = "";
    var a = objek.value;
    var b = a.replace(/[^\d]/g, "");
    var c = "";
    var panjang = b.length;
    var j = 0; for (var i = panjang; i > 0; i--) {
        j = j + 1; if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i - 1, 1) + separator + c;
        } else {
            c = b.substr(i - 1, 1) + c;
        }
    } objek.value = c;
}

function bsNumFormatClear(data) {
    return data.replace(/\./g,'');
}

function bsNumFormat(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

var KTAppOptions = {
    "colors": {
        "state": {
            "brand": "#591df1",
            "light": "#ffffff",
            "dark": "#282a3c",
            "primary": "#5867dd",
            "success": "#34bfa3",
            "info": "#36a3f7",
            "warning": "#ffb822",
            "danger": "#fd3995"
        },
        "base": {
            "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
            "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
        }
    }
};
</script>