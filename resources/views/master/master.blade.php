<!DOCTYPE html>

<html lang="en">
	@include('master.head')
	
	<div class="bsloader-container" style="display:none;">
		<div class="bsprogress bsfloat bsshadow">
		<div class="bsprogress__item"></div>
		</div>
	</div>
	<body id="bg_new" class="scrollStyle kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">

    @include('component.zn-iconbox')

		<!-- begin:: Header Mobile -->
		<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
			<div class="kt-header-mobile__logo">
				<a href="">
					<img style="width:40px;" class="img-fluid" alt="Logo" src="" />
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
			</div>
		</div>

		<!-- end:: Header Mobile -->
		<div class="kt-grid kt-grid--hor kt-grid--root">
		

			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					<!-- begin:: Header -->
					<div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
						<div class="kt-container  kt-container--fluid ">

							<!-- begin: Header Menu -->
							<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

							<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
								<button class="kt-aside-toggler kt-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
								<a class="kt-header__brand-logo" href="#">
                                    <div style="display: inline-block;">
                                        <img alt="Logo" src="{{asset('img/logo.png')}}" style="width: 60px;margin-top: -25px;" />
                                    </div>
                                    <div style="display: inline-block;">
                                        <span class="zn-text-logo" style="display: block;">SEED TEMPLATE</span>
                                        <span class="zn-text-logo-sub">"Simple & Inovative Solution" </span>
                                    </div>
                                </a>

							</div>

							<!-- begin:: Header Topbar -->
							<div class="kt-header__topbar kt-grid__item">

								<!--begin: User bar -->
								<div class="kt-header__topbar-item kt-header__topbar-item--user">
									<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
										<span class="kt-header__topbar-welcome kt-visible-desktop">Hi, {{{Auth::user()->username}}}</span>
                                    <span class="kt-header__topbar-username kt-visible-desktop"></span>
										{{-- <img alt="Pic" src="" style="
                                        width: 42px;
                                        border: 2px solid #7b7b7b;
                                        border-radius: 10px;
                                    	"/> --}}

                                        <i class="flaticon-user" style="
                                        font-size: 30px;
                                        background: #f5f6fc;
                                        padding: 2px 10px;
                                        border-radius: 5px;
                                        margin-left: 10px;
                                    "></i>
										<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
									</div>
									<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

										<!--begin: Head -->
										<div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
											<div class="kt-user-card__avatar">
												<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
											</div>
											<div class="kt-user-card__name">
                                                {{{Auth::user()->name}}}
											</div>
											<div class="kt-user-card__badge">
												@php
													$userRole = \DB::table('ref_role_user')->where('role_id', Auth::user()->role)->first();
												@endphp
												<span class="btn btn-label-primary btn-sm btn-bold btn-font-md">{{ $userRole->role_name }}</span>
											</div>
										</div>

										<!--end: Head -->

										<!--begin: Navigation -->
										<div class="kt-notification">
											<div class="kt-notification__custom kt-space-between">
												<form action="{{ route('logout.action') }}" method="post">
													{{ csrf_field() }}
													<input type="submit" value="Sign Out" class="btn btn-label btn-label-brand btn-sm btn-bold">
												</form>
											</div>
										</div>
										<!--end: Navigation -->
									</div>
								</div>

							</div>

						</div>
					</div>

					<!-- end:: Header -->

					<!-- begin:: Aside -->
					<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
					<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

						<!-- begin:: Aside Menu -->
                            @include('master.menu')
                        <!-- end:: Aside Menu -->

					</div>

					<!-- end:: Aside -->
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
							<div id="pageLoad">
								@yield('content')
							</div>
						</div>
					</div>

					<!-- begin:: Footer -->
					<div class="kt-footer kt-grid__item" id="kt_footer">
						<div class="kt-container ">
							<div class="kt-footer__wrapper">
								<div class="kt-footer__copyright">
									2019&nbsp;&copy;&nbsp; Basys</a>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>

	</body>

</html>
