<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid"
{{-- style="background: url('{{ asset('img/pat1.png') }}');" --}}
id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
        <ul class="kt-menu__nav mt-4">
            
            @php
                $getMenu = \DB::select("SELECT * from ref_menu order by seq");
            @endphp

            @foreach ($getMenu as $p)
                @if ($p->parent == null)
                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    @if ($p->menu_action)
                        <a onclick="bsLoadMenu('{{$p->menu_action}}')"  class="kt-menu__link">
                    @else
                        <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                    @endif
                            {!!$p->menu_icon!!}
                            <span class="kt-menu__link-text">{{$p->menu_name}}</span>
                            {!!($p->menu_action) ? '':'<i class="kt-menu__ver-arrow la la-angle-right"></i>' !!}
                        </a>

                        @foreach ($getMenu as $sub)
                            @if ($sub->parent == $p->id)
                                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item " aria-haspopup="true">
                                        @if ($sub->menu_action)
                                            <a onclick="bsLoadMenu('{{$sub->menu_action}}')"  class="kt-menu__link">
                                        @else
                                            <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                        @endif   
                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                                <span class="kt-menu__link-text">{{$sub->menu_name}}</span>
                                                {!!($sub->menu_action) ? '':'<i class="kt-menu__ver-arrow la la-angle-right"></i>' !!}
                                            </a>

                                            @foreach ($getMenu as $subChild)
                                                @if ($subChild->parent == $sub->id)
                                                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                                        <ul class="kt-menu__subnav">
                                                            <li class="kt-menu__item " aria-haspopup="true">
                                                            @if ($subChild->menu_action)
                                                                <a onclick="bsLoadMenu('{{$subChild->menu_action}}')"  class="kt-menu__link">
                                                            @else
                                                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                                            @endif   
                                                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                                                    <span class="kt-menu__link-text">{{$subChild->menu_name}}</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            @endforeach
                                            
                                        </li>
                                    </ul>
                                </div>
                            @endif
                        @endforeach

                    </li>
                @endif
                   
            @endforeach

        
        </ul>
    </div>
</div>
