<?php
Auth::routes();

Route::get('/', 'LoginController@index')->name('login.view');
Route::post('/login/action', 'LoginController@authenticate')->name('login.action');
Route::post('/logout', 'LoginController@logout')->name('logout.action');

Route::group(['middleware' => 'auth'], function () {

    // DASHBOARD
    Route::get('dashboard', 'HomeController@index')->name('home.index');
    Route::post('get_dashboard', 'HomeController@get_dashboard')->name('home.get_dashboard');

    // USER
    Route::prefix('user')->group(function () {
        Route::get('index', 'UserController@index')->name('user.index');
        Route::get('data', 'UserController@data')->name('user.data');
        Route::post('edit', 'UserController@edit')->name('user.edit');
        Route::post('store', 'UserController@store')->name('user.store');
        Route::delete('delete/{id}', 'UserController@delete')->name('user.delete');
        Route::post('active','UserController@SetActive')->name('user.setActive');
        Route::post('resetpassword', 'UserController@resetPassword')->name('user.resetPassword');
        Route::post('changePassword', 'UserController@changePassword')->name('user.changePassword');
        Route::get('check/password', 'UserController@check_password')->name('password.check');
    });

    // REF
    Route::prefix('ref')->group(function () {
        Route::get('index', 'RefController@index')->name('ref.index');
        Route::get('data', 'RefController@data')->name('ref.data');
        Route::post('edit', 'RefController@edit')->name('ref.edit');
        Route::post('store', 'RefController@store')->name('ref.store');
        Route::post('store/custom', 'RefController@storeCustom')->name('ref.storeCustom');
        Route::post('active','RefController@SetActive')->name('ref.setActive');
        Route::delete('delete/{id}', 'RefController@delete')->name('ref.delete');
    });

});


Route::get('valid/checking/{id}', 'Controller@checking')->name('valid.checking');
Route::get('select/{id}', 'Controller@getSelect')->name('c.getSelect');
